# Cloud Automation Script

## Getting started

The automation process involves the use of Systems Manager Run Command RunShellScript, parameter store, Systems Manager document and most importantly Bash script stored in Repository.  Bash script will be downloaded at /var location on the Bastion host and variables.txt file will also be created at /var location of Bastion host by Run Document functionality of Systems manager. The script and variables file will be copied to the private instance using scp command from Bastion host and finally the script will be run on the private instance. Bash script will clone the repo to the private instance defined in a common folder located at /var/www/uims. The name of universities shall be passed on to the script as a variable (Stored in the parameter store of AWS) holding the list of universities separated by comma. Bash script shall then run through a loop for each university name and do following functions:
- Create corresponding www folder at /var/www/university_name/uims and html folder at /var/www/html/university_name/uims.
- Generate db name, db username, db password using openssl rand command in predefined format.
- Create vhosts file for each university in /etc/conf.d with university_name.conf
- Create corresponding databases and Users along with related permissions.

## A) Create SSM Document with name clouddeploymentdoc. If already exists then ignore this step.

```
To create an SSM document (console)
1.	Open the AWS Systems Manager console at https://console.aws.amazon.com/systems-manager/.
2.	In the navigation pane, choose Documents.
        -or-
        If the AWS Systems Manager home page opens first, choose the menu icon (   ) to open the navigation pane, and then choose Documents in the navigation pane.
3.	Select Create document and select Command or Session.
4.	Enter a descriptive name for the document-clouddeploymentdoc.
5.	(Optional) For Target type, leave this blank.
6.	In the Document type list, choose the type of document as Command document.
7.	Delete the brackets in the Content field of YAML format, and then paste the document content available here in Repository with name clouddeploymentdoc.yaml.
8.	(Optional) In the Document tags section, apply one or more tag key name/value pairs to the document.
9.	Choose Create document to save the document.

```

## B) Create SSM parameter 

```
To create a parameter (console)
1.	Open the AWS Systems Manager console at https://console.aws.amazon.com/systems-manager/.
2.	In the navigation pane, choose Parameter Store.
        -or-
        If the AWS Systems Manager home page opens first, choose the menu icon (   ) to open the navigation pane, and then choose Parameter Store.
3.	Choose Create parameter.
4.	In the Name box, enter the name for the parameter. For example, enter phase4universitieslist. 
5.	(Optional) In the Description box, type a description if required.
6.	For Parameter tier choose Standard.
7.	For Type, choose String, and Data type choose 'text'.
8.	In the Value box, type Universities list separated by comma. For University name use URL of the university without samarth.ac.in for unique-ness of the folder structure. For example, type univ1, univ2, univ3, univ4, univ5 etc.
9.	(Optional) In the Tags area, apply one or more tag key-value pairs to the parameter.
10.	Choose Create parameter.


```
## C) Systems Manager Prerequisites.

```
1.	Ensure that amazon-ssm-agent is running on the Bastion Host. On all Amazon Linux Base AMIs dated 2017.09 and later are preinstalled with amazon-ssm-agent.
2.	For Amazon EC2 instances, create an IAM instance profile role having AmazonSSMManagedInstanceCore and AmazonEC2RoleforSSM policy and attach it to the instance for the duration this script is to be run on the instance. In this case this role needs to be given to Bastion Host.

```

## D) Execute the Document created at step A) from RUN Command feature of Systems Manager.

```
To run a SSM document
1.	Open the Amazon Systems Manager console at https://console.amazonaws.cn/systems-manager/.
2.	In the navigation pane, choose Run Command.
        -or-
        If the Amazon Systems Manager home page opens first, choose the menu icon (   ) to open the navigation pane, and then choose Run Command.
3.	Choose Run command.
4.	In the Document list, search by the name of SSM document created at step A) and select it.
5.	In the Document Version select Latest Version at runtime
6.	In the Command Parameters, check whether the values filled by default are OK or not. If the values need to be changed change them in the space provided. Verify the values of parameters again. Here the parameters of type string can be referred from Parameter store of Systems Manager in the format given here: {{ssm:parameter-name}}.
7.	In the Targets section, choose the Bastion Host node on which you want to run this operation by specifying tags or selecting instances manually.
8.	For Other parameters:
	For Comment, enter information about this command.
        For Timeout (seconds), specify the number of seconds for the system to wait before failing the overall command execution.
9.	(Optional) For Output options, to save the command output to a file, select the Write command output to an S3 bucket box. Enter the bucket and prefix (folder) names in the boxes.



```
## E) Final Cleanup of files from the instance.

Use the RUN Command utility of Systems Manager and select AWS-RunShellScript to cleanup the files on Bastion Host. In the Command parameters enter the following command.
```
rm -f /var/script.sh
rm -f /var/variables.txt
rm -f /var/rdsdbconfig.cnf
```
***

# Function of SSM Document.

SSM Document created in step A) does the following functions.
1. It downloads the bash script from Repository on to the Bastion Host at /var location with the name script.sh.
2. It passes on the variables entered manually during RUN command process and parameters from parameter store to variables.txt file at /var location of the Bastion Host.
3. It gives executable permission to the script.
4. It copies the script.sh file and variables.txt file from Bastion Host to Private Instance at the location /home/ec2-user.
5. It executes the script transferred to the private instance with sudo privilege.

# Function of Bash Script.
Bash script is stored in the repository. All the variables passed on to the variables.txt file by SSM document are made available to the script.Script then creates folder structure as required for all the universities passed on through SSM parameter, creates Database Username and Password along with substitution in the relevant files. Vhosts file of each university s also created at /etc/httpd/conf.d locations. If Dbworktobedoneornot parameter is set to yes then concerned database and database users are also created on the RDS host.

